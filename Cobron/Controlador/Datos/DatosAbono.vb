﻿Public Class DatosAbono
    Inherits Datos
    Public IdAbono, IdFactura, Fecha, Abono As DatoBdD

    Public Overrides Sub Limpiar()
        IdAbono.Limpiar()
        IdFactura.Limpiar()
        Fecha.Limpiar()
        Abono.Limpiar()
    End Sub

    Public Overrides Sub TomarEnCuentaTodo(TomarEnCuenta As Boolean)
        IdFactura.TomarEnCuenta = TomarEnCuenta
        IdAbono.TomarEnCuenta = TomarEnCuenta
        Fecha.TomarEnCuenta = TomarEnCuenta
        Abono.TomarEnCuenta = TomarEnCuenta
    End Sub

    Public Overrides Function Clone() As Object
        Dim Yo As New DatosAbono
        With Yo
            .IdAbono = IdAbono
            .IdFactura = IdFactura
            .Fecha = Fecha
            .Abono = Abono
        End With
        Return Yo
    End Function
End Class
