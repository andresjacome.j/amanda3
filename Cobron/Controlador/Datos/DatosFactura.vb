﻿Public Class DatosFactura
    Inherits Datos
    Public IdMoroso, IdFactura, Fecha, Total As DatoBdD
    Public Overrides Sub Limpiar()
        IdMoroso.Limpiar()
        IdFactura.Limpiar()
        Fecha.Limpiar()
        Total.Limpiar()
    End Sub

    Public Overrides Sub TomarEnCuentaTodo(TomarEnCuenta As Boolean)
        IdMoroso.TomarEnCuenta = TomarEnCuenta
        IdFactura.TomarEnCuenta = TomarEnCuenta
        Fecha.TomarEnCuenta = TomarEnCuenta
        Total.TomarEnCuenta = TomarEnCuenta
    End Sub

    Public Overrides Function Clone() As Object
        Dim Yo As New DatosFactura
        With Yo
            .IdFactura = IdFactura
            .IdMoroso = IdMoroso
            .Total = Total
            .Fecha = Fecha
        End With
        Return Yo
    End Function
End Class
