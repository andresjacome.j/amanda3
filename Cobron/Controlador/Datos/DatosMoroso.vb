﻿Public Class DatosMoroso
    Inherits Datos
    Public IdMoroso, RUC, RazonSocial, Direccion As DatoBdD
    Public Overrides Sub Limpiar()
        IdMoroso.Limpiar()
        RUC.Limpiar()
        RazonSocial.Limpiar()
        Direccion.Limpiar()
    End Sub

    Public Overrides Sub TomarEnCuentaTodo(TomarEnCuenta As Boolean)
        IdMoroso.TomarEnCuenta = TomarEnCuenta
        RUC.TomarEnCuenta = TomarEnCuenta
        RazonSocial.TomarEnCuenta = TomarEnCuenta
        Direccion.TomarEnCuenta = TomarEnCuenta
    End Sub

    Public Overrides Function Clone() As Object
        Dim Yo As New DatosMoroso
        With Yo
            .RazonSocial = RazonSocial
            .RUC = RUC
            .IdMoroso = IdMoroso
            .Direccion = Direccion
        End With
        Return Yo
    End Function
End Class
