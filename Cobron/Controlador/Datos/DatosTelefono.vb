﻿Public Class DatosTelefono
    Inherits Datos
    Public IdTelefono, IdMoroso, Telefono As DatoBdD
    Public Overrides Sub Limpiar()
        IdTelefono.Limpiar()
        IdMoroso.Limpiar()
        Telefono.Limpiar()
    End Sub

    Public Overrides Sub TomarEnCuentaTodo(TomarEnCuenta As Boolean)
        IdTelefono.TomarEnCuenta = TomarEnCuenta
        IdMoroso.TomarEnCuenta = TomarEnCuenta
        Telefono.TomarEnCuenta = TomarEnCuenta
    End Sub

    Public Overrides Function Clone() As Object
        Dim Yo As New DatosTelefono
        With Yo
            .IdMoroso = IdMoroso
            .IdTelefono = IdTelefono
            .Telefono = Telefono
        End With
        Return Yo
    End Function
End Class
