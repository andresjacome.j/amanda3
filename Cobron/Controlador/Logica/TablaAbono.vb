﻿Imports Cobron

Public Class TablaAbono
    Inherits Tabla
    Public Sub New()
        NombreTabla = "Abono"
    End Sub
    Public Sub New(ByRef Con As ConexionSQL)
        MyBase.New(Con)
        NombreTabla = "Abono"
    End Sub
    Protected Overrides Sub IgualarDatos(ByRef Dts As Datos)
        If Dts Is Nothing Then Exit Sub
        Dim D As DatosAbono = Dts
        With SQLExe.Parameters
            .Clear()
            If D.IdFactura.TomarEnCuenta Then .AddWithValue("@IdFactura", D.IdFactura.Valor)
            If D.IdAbono.TomarEnCuenta Then .AddWithValue("@IdAbono", D.IdAbono.Valor)
            If D.Fecha.TomarEnCuenta Then .AddWithValue("@Fecha", D.Fecha.Valor)
            If D.Abono.TomarEnCuenta Then .AddWithValue("@Abono", D.Abono.Valor)
        End With
    End Sub

    Public Overrides Function Editar(ByRef Dts As Datos) As Boolean
        Throw New NotImplementedException()
    End Function

    Public Overrides Function Eliminar(ByRef Dts As Datos) As Boolean
        Throw New NotImplementedException()
    End Function

    Public Overrides Function Insertar(ByRef Dts As Datos) As Boolean
        SQLExe.CommandText = "InsertarAbono"
        SQLExe.CommandType = CommandType.StoredProcedure
        Call IgualarDatos(Dts)
        Return EjecutarCadSQL()
    End Function

    Public Overrides Function SeleccionarTodo(ByRef Optional Dts As Datos = Nothing) As DataTable
        Throw New NotImplementedException()
    End Function
    Public Function MostrarAbonoFactura(ByRef Dts As DatosAbono) As DataTable
        SQLExe.CommandText = "MostrarAbonoFactura"
        SQLExe.CommandType = CommandType.StoredProcedure
        Call IgualarDatos(Dts)
        Return Mostrar()
    End Function
End Class
