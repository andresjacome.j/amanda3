﻿Imports Cobron

Public Class TablaFactura
    Inherits Tabla
    Public Sub New()
        NombreTabla = "Factura"
    End Sub
    Public Sub New(ByRef Con As ConexionSQL)
        MyBase.New(Con)
        NombreTabla = "Factura"
    End Sub
    Protected Overrides Sub IgualarDatos(ByRef Dts As Datos)
        If Dts Is Nothing Then Exit Sub
        Dim D As DatosFactura = Dts
        With SQLExe.Parameters
            .Clear()
            If D.IdMoroso.TomarEnCuenta Then .AddWithValue("@IdMoroso", D.IdMoroso.Valor)
            If D.IdFactura.TomarEnCuenta Then .AddWithValue("@IdFactura", D.IdFactura.Valor)
            If D.Total.TomarEnCuenta Then .AddWithValue("@Total", D.Total.Valor)
            If D.Fecha.TomarEnCuenta Then .AddWithValue("@Fecha", D.Fecha.Valor)
        End With
    End Sub

    Public Overrides Function Editar(ByRef Dts As Datos) As Boolean
        Throw New NotImplementedException()
    End Function

    Public Overrides Function Eliminar(ByRef Dts As Datos) As Boolean
        Throw New NotImplementedException()
    End Function

    Public Overrides Function Insertar(ByRef Dts As Datos) As Boolean
        Throw New NotImplementedException()
    End Function

    Public Overrides Function SeleccionarTodo(ByRef Optional Dts As Datos = Nothing) As DataTable
        Throw New NotImplementedException()
    End Function
    Public Function MostrarFacturaIndividual(ByRef Dts As DatosFactura) As DataTable
        SQLExe.CommandText = "MostrarFacturaIndividual"
        SQLExe.CommandType = CommandType.StoredProcedure
        Call IgualarDatos(Dts)
        Return Mostrar()
    End Function
End Class
