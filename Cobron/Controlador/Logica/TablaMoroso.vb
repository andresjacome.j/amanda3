﻿Public Class TablaMoroso
    Inherits Tabla
    Public Sub New()
        NombreTabla = "Moroso"
    End Sub
    Public Sub New(ByRef Con As ConexionSQL)
        MyBase.New(Con)
        NombreTabla = "Moroso"
    End Sub
    Protected Overrides Sub IgualarDatos(ByRef Dts As Datos)
        If Dts Is Nothing Then Exit Sub
        Dim D As DatosMoroso = Dts
        With SQLExe.Parameters
            .Clear()
            If D.IdMoroso.TomarEnCuenta Then .AddWithValue("@IdMoroso", D.IdMoroso.Valor)
            If D.RUC.TomarEnCuenta Then .AddWithValue("@RUC", D.RUC.Valor)
            If D.RazonSocial.TomarEnCuenta Then .AddWithValue("@RazonSocial", D.RazonSocial.Valor)
            If D.Direccion.TomarEnCuenta Then .AddWithValue("@Direccion", D.Direccion.Valor)
        End With
    End Sub

    Public Overrides Function Editar(ByRef Dts As Datos) As Boolean
        SQLExe.CommandText = "ActualizarUsuario"
        SQLExe.CommandType = CommandType.StoredProcedure
        Call IgualarDatos(Dts)
        Return EjecutarCadSQL()
    End Function

    Public Overrides Function Eliminar(ByRef Dts As Datos) As Boolean
        SQLExe.CommandText = "EliminarUsuario"
        SQLExe.CommandType = CommandType.StoredProcedure
        Call IgualarDatos(Dts)
        Return EjecutarCadSQL()
    End Function

    Public Overrides Function Insertar(ByRef Dts As Datos) As Boolean
        SQLExe.CommandText = "InsertarUsuario"
        SQLExe.CommandType = CommandType.StoredProcedure
        Call IgualarDatos(Dts)
        Return EjecutarCadSQL()
    End Function

    Public Overrides Function SeleccionarTodo(ByRef Optional Dts As Datos = Nothing) As DataTable
        SQLExe.CommandText = "MostrarUsuario"
        SQLExe.CommandType = CommandType.StoredProcedure
        Call IgualarDatos(Dts)
        Return Mostrar()
    End Function
    Public Function ExisteUsuario(ByRef Dts As Datos) As Integer
        SQLExe.CommandText = "ExisteUsuario"
        SQLExe.CommandType = CommandType.StoredProcedure
        Call IgualarDatos(Dts)
        Dim T As DataTable = Mostrar()
        If T Is Nothing Then Return -1 'Error
        Return Val(T.Rows(0).Item(0))
    End Function
    Public Function MostrarDatosMoroso(ByRef Dts As DatosMoroso) As DataTable
        SQLExe.CommandText = "MostrarDatosMoroso"
        SQLExe.CommandType = CommandType.StoredProcedure
        Call IgualarDatos(Dts)
        Return Mostrar()
    End Function
    Public Function MostrarTelefono(ByRef Dts As DatosMoroso) As DataTable
        SQLExe.CommandText = "MostrarTelefono"
        SQLExe.CommandType = CommandType.StoredProcedure
        Call IgualarDatos(Dts)
        Return Mostrar()
    End Function
    Public Function MostrarFacturasMoroso(ByRef Dts As DatosMoroso) As DataTable
        SQLExe.CommandText = "MostrarFacturasMoroso"
        SQLExe.CommandType = CommandType.StoredProcedure
        Call IgualarDatos(Dts)
        Return Mostrar()
    End Function
End Class
