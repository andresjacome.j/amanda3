﻿Imports Cobron

Public Class TablaTelefono
    Inherits Tabla
    Public Sub New()
        NombreTabla = "Telefono"
    End Sub
    Public Sub New(ByRef Con As ConexionSQL)
        MyBase.New(Con)
        NombreTabla = "Telefono"
    End Sub
    Protected Overrides Sub IgualarDatos(ByRef Dts As Datos)
        If Dts Is Nothing Then Exit Sub
        Dim D As DatosTelefono = Dts
        With SQLExe.Parameters
            .Clear()
            If D.IdMoroso.TomarEnCuenta Then .AddWithValue("@IdMoroso", D.IdMoroso.Valor)
            If D.IdTelefono.TomarEnCuenta Then .AddWithValue("@IdTelefono", D.IdTelefono.Valor)
            If D.Telefono.TomarEnCuenta Then .AddWithValue("@Telefono", D.Telefono.Valor)
        End With
    End Sub

    Public Overrides Function Editar(ByRef Dts As Datos) As Boolean
        Throw New NotImplementedException()
    End Function

    Public Overrides Function Eliminar(ByRef Dts As Datos) As Boolean
        Throw New NotImplementedException()
    End Function

    Public Overrides Function Insertar(ByRef Dts As Datos) As Boolean
        Throw New NotImplementedException()
    End Function

    Public Overrides Function SeleccionarTodo(ByRef Optional Dts As Datos = Nothing) As DataTable
        Throw New NotImplementedException()
    End Function
End Class
