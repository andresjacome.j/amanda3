﻿Public Class ControladorFactura
    Public Shared Function ObtenerFactura(IdFactura As Integer) As DataTable
        Dim DF As New DatosFactura
        DF.IdFactura.Valor = IdFactura
        Return (New TablaFactura).MostrarFacturaIndividual(DF)
    End Function
    Public Shared Function ObtenerAbonos(IdFactura As Integer) As DataTable
        Dim DA As New DatosAbono
        DA.IdFactura.Valor = IdFactura
        Return (New TablaAbono).MostrarAbonoFactura(DA)
    End Function
    Public Shared Function RegistrarAbono(IdFactura As Integer, Valor As Double) As Boolean
        If Valor = 0 Then Return False
        Dim DA As New DatosAbono
        DA.IdFactura.Valor = IdFactura
        DA.Abono.Valor = Valor
        DA.Fecha.Valor = Date.Now
        Return (New TablaAbono).Insertar(DA)
    End Function
End Class
