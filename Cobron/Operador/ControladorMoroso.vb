﻿Public Class ControladorMoroso
    Public Shared Function ObtenerDatosPersonales(Ruc As String) As DataTable
        Ruc = Ruc.Trim
        If Ruc = "" Then Return Nothing
        Dim DM As New DatosMoroso
        DM.RUC.Valor = Ruc
        Return (New TablaMoroso).MostrarDatosMoroso(DM)
    End Function
    Public Shared Function ObtenerContacto(Ruc As String) As DataTable
        Ruc = Ruc.Trim
        If Ruc = "" Then Return Nothing
        Dim DM As New DatosMoroso
        DM.RUC.Valor = Ruc
        Return (New TablaMoroso).MostrarTelefono(DM)
    End Function
    Public Shared Function ObtenerFacturas(Ruc As String) As DataTable
        Ruc = Ruc.Trim
        If Ruc = "" Then Return Nothing
        Dim DM As New DatosMoroso
        DM.RUC.Valor = Ruc
        Return (New TablaMoroso).MostrarFacturasMoroso(DM)
    End Function
End Class
