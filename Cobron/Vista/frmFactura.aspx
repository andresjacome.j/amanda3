﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmFactura.aspx.vb" Inherits="Cobron.frmFactura" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    <style type="text/css">
        .auto-style1 {
            width: 100%;
        }
        .auto-style3 {
            width: 129px;
        }
        .auto-style5 {
            width: 187px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
        <asp:Panel ID="panelFactura" runat="server">
            <table class="auto-style1">
                <tr>
                    <td class="auto-style3">
                        <asp:Label ID="Label1" runat="server" Text="Factura:"></asp:Label>
                    </td>
                    <td>
                        <asp:GridView ID="gridFactura" runat="server" BackColor="White" BorderColor="#999999" BorderStyle="Solid" BorderWidth="1px" CellPadding="3" ForeColor="Black" GridLines="Vertical">
                            <AlternatingRowStyle BackColor="#CCCCCC" />
                            <FooterStyle BackColor="#CCCCCC" />
                            <HeaderStyle BackColor="Black" Font-Bold="True" ForeColor="White" />
                            <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                            <SelectedRowStyle BackColor="#000099" Font-Bold="True" ForeColor="White" />
                            <SortedAscendingCellStyle BackColor="#F1F1F1" />
                            <SortedAscendingHeaderStyle BackColor="#808080" />
                            <SortedDescendingCellStyle BackColor="#CAC9C9" />
                            <SortedDescendingHeaderStyle BackColor="#383838" />
                        </asp:GridView>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style3">
                        <asp:Label ID="Label2" runat="server" Text="Operaciones:"></asp:Label>
                    </td>
                    <td>
                        <asp:Button ID="cmdPago" runat="server" Text="Abonos" />
                        <asp:Button ID="cmdPago0" runat="server" Text="Cobranza" />
                    </td>
                </tr>
            </table>
        </asp:Panel>
    
    </div>
        <asp:Panel ID="panelPago" runat="server">
            <table class="auto-style1">
                <tr>
                    <td class="auto-style5">
                        <asp:Label ID="Label3" runat="server" Text="Historial de abonos:"></asp:Label>
                    </td>
                    <td>
                        <asp:GridView ID="gridAbonos" runat="server" BackColor="White" BorderColor="#999999" BorderStyle="Solid" BorderWidth="1px" CellPadding="3" ForeColor="Black" GridLines="Vertical">
                            <AlternatingRowStyle BackColor="#CCCCCC" />
                            <FooterStyle BackColor="#CCCCCC" />
                            <HeaderStyle BackColor="Black" Font-Bold="True" ForeColor="White" />
                            <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                            <SelectedRowStyle BackColor="#000099" Font-Bold="True" ForeColor="White" />
                            <SortedAscendingCellStyle BackColor="#F1F1F1" />
                            <SortedAscendingHeaderStyle BackColor="#808080" />
                            <SortedDescendingCellStyle BackColor="#CAC9C9" />
                            <SortedDescendingHeaderStyle BackColor="#383838" />
                        </asp:GridView>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style5">
                        <asp:Label ID="Label4" runat="server" Text="Nuevo abono:"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="txtValorAbono" runat="server" Width="232px"></asp:TextBox>
                        <asp:Button ID="cmdRegistrarAbono" runat="server" Text="Registrar" />
                        <asp:Label ID="lblEstadoAbono" runat="server" Text="Estado"></asp:Label>
                    </td>
                </tr>
            </table>
        </asp:Panel>
        <asp:Panel ID="panelCobranza" runat="server" Visible="False">
        </asp:Panel>
    </form>
</body>
</html>
