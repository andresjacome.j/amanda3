﻿Public Class frmFactura
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Call ActualizarVistaFactura()
            Call ActualizarVistaAbonos()
        End If
    End Sub
    Private Sub ActualizarVistaFactura()
        gridFactura.DataSource = ControladorFactura.ObtenerFactura(Request.Params("idf"))
        gridFactura.DataBind()
    End Sub
    Private Sub ActualizarVistaAbonos()
        gridAbonos.DataSource = ControladorFactura.ObtenerAbonos(Request.Params("idf"))
        gridAbonos.DataBind()
    End Sub
    Protected Sub cmdPago_Click(sender As Object, e As EventArgs) Handles cmdPago.Click
        panelPago.Visible = True
        panelCobranza.Visible = False
    End Sub

    Protected Sub cmdPago0_Click(sender As Object, e As EventArgs) Handles cmdPago0.Click
        panelPago.Visible = False
        panelCobranza.Visible = True
    End Sub

    Protected Sub cmdRegistrarAbono_Click(sender As Object, e As EventArgs) Handles cmdRegistrarAbono.Click
        If ControladorFactura.RegistrarAbono(Request.Params("idf"), txtValorAbono.Text) Then
            txtValorAbono.Text = ""
            lblEstadoAbono.Text = "Abono registrado."
            Call ActualizarVistaFactura()
            Call ActualizarVistaAbonos()
        Else
            lblEstadoAbono.Text = "Error. Abono no registrado."
        End If
    End Sub
End Class