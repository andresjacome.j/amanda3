﻿Public Class frmPrincipal
    Inherits Page
    Protected Sub cmdConsultar_Click(sender As Object, e As EventArgs) Handles cmdConsultar.Click
        gridDatos.DataSource = ControladorMoroso.ObtenerDatosPersonales(txtCedula.Text.Trim)
        gridDatos.DataBind()
        gridContacto.DataSource = ControladorMoroso.ObtenerContacto(txtCedula.Text.Trim)
        gridContacto.DataBind()
        gridFacturas.DataSource = ControladorMoroso.ObtenerFacturas(txtCedula.Text.Trim)
        gridFacturas.DataBind()
        cmdGastionar.Visible = gridFacturas.DataSource IsNot Nothing
    End Sub

    Protected Sub cmdGastionar_Click(sender As Object, e As EventArgs) Handles cmdGastionar.Click
        If gridFacturas.SelectedRow IsNot Nothing Then
            Response.Redirect("frmFactura.aspx?idf=" & gridFacturas.SelectedRow.Cells(1).Text.ToString)
        End If
    End Sub
End Class