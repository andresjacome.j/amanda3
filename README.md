Proyecto para el desarrollo del Sistema de Gestión de Cobros "AMANDA 3"



Requisitos iniciales del Proyecto:
- Se asume que el sistema recibe un listado de facturas que se encuentran en mora, por cliente

- Permite consultar cliente por cédula o RUC

- Aparece entonces las facturas que debe dicho cliente

- Datos del cliente: 
	- cédula o RUC
	- nombre o razón social
	- al menos 2 teléfonos de referencia
	- dirección

- Cuando se seleccione una de las facturas se podrán hacer 2 cosas:

  1. Registrar abonos de deuda 

  2. Gestión de cobros
	La gestión de cobros puede ser de 2 tipos:

	2.1. Telefónica
		Posibles resultados:
		- El cliente no contesta
		- Se agendó cita para pago o abono de deuda

	2.2. Domiciliaria
		Posibles resultados:
		- El cliente no está en casa
		- El cliente pagó abono
		- El cliente pagó totaidad de deuda

- Por cada factura se pueden tener varios registros de gestión de cobros.

- La Gestión de cobros deberá tener:

	- quién gestionó el cobro (el usuario registrado)
	- fecha de realización
	- fecha de compromiso de pago
	- observaciones
	
