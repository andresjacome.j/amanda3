USE MASTER
DROP DATABASE AMANDA3
CREATE DATABASE  AMANDA3
USE AMANDA3

Create table [Usuario]
(
	[id_Usuario] Char(3) NOT NULL,
	[nombre_Usuario] Varchar(30) NOT NULL,
Primary Key ([id_Usuario])
) 
go

Create table [Cliente]
(
	[id_Cliente] Char(3) NOT NULL,
	[cedula_Cliente] Char(10) NOT NULL,
	[nombre_Cliente] Varchar(30) NOT NULL,
	[direccion_Cliente] Varchar(50) NOT NULL,
Primary Key ([id_Cliente])
) 

Create table [Telefono]
(
	[id_Telefono] Char(3) NOT NULL,
	[id_Cliente] Char(3) NOT NULL,
	[tipo_Telefono] Char(20) NOT NULL,
	[numero_Telefono] Varchar(15) NOT NULL,
Primary Key ([id_Telefono])
) 

Create table [Factura]
(
	[id_Factura] Char(3) NOT NULL,
	[id_Cliente] Char(3) NOT NULL,
	[monto_Factura] Money NOT NULL,
Primary Key ([id_Factura])
) 

Create table [Abono]
(
	[id_Abono] Char(3) NOT NULL,
	[id_Factura] Char(3) NOT NULL,
	[fecha_Abono] Date NOT NULL,
	[monto_Abono] Money NOT NULL,
Primary Key ([id_Abono])
) 

Create table [Gestion_Cobro]
(
	[id_Gestion] Char(3) NOT NULL,
	[id_Usuario] Char(3) NOT NULL,
	[id_Factura] Char(3) NOT NULL,
	[fecha_Gestion] Date NOT NULL,
	[fecha_Compromiso] Date NOT NULL,
	[id_Tipo] Char(3) NOT NULL,
	[observacion_Gestion] Varchar(100) NOT NULL,
Primary Key ([id_Gestion])
) 

Create table [Tipo_Gestion]
(
	[id_Tipo] Char(3) NOT NULL,
	[tipo_Gestion] Varchar(30) NOT NULL,
	[resultado] Varchar(50) NOT NULL,
Primary Key ([id_Tipo])
) 


Alter table [Telefono] add foreign Key ([id_Cliente]) references [Cliente]([id_Cliente])
go

Alter table [Factura] add foreign Key ([id_Cliente]) references [Cliente]([id_Cliente])
go

Alter table [Abono] add foreign Key ([id_Factura]) references [Factura]([id_Factura])
go

Alter table [Gestion_Cobro] add foreign Key ([id_Usuario]) references [Usuario]([id_Usuario])
go

Alter table [Gestion_Cobro] add foreign Key ([id_Factura]) references [Factura]([id_Factura])
go

Alter table [Gestion_Cobro] add foreign Key ([id_Tipo]) references [Tipo_Gestion]([id_Tipo])
go

